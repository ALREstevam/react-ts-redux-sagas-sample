export interface FactStatus {
  verified: boolean
  sentCount: number
}

export interface CatFactResponse {
  status: FactStatus
  type: string | 'cat'
  text: string
  deleted: boolean
  _id: string
  user: string
  source: string | 'user'
  updatedAt: string
  createdAt: string
  used: boolean
}

export interface CatFacts extends CatFactResponse {

}