export function getJsonFromLocalStorage<T>(key: string) {
  const data = localStorage.getItem(key)
  if (data) {
    return JSON.parse(data) as T
  }
}