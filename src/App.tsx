import React from 'react';
import { Provider } from "react-redux";
import './styles/App.css';
import store from './store/store';
import CatFactsScreen from './screens/CatFacts/CatFactsScreen';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <CatFactsScreen title='Cat facts!'/>
      </Provider>
    </div>
  );
}

export default App;
