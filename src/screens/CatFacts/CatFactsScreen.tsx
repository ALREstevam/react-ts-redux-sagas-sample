import React, { useCallback/*, useEffect*/ } from 'react'
import { StoreState, Types } from '../../store/ducks/catFactsActions';
import { useSelector, useDispatch } from "react-redux";
import './styles.css'
import catFactsStorage from '../../services/catFactsStorage';


interface Props {
  title: string
}

export default function CatFactsScreen({ title }: Props) {

  const state: StoreState = useSelector((state: {catFactsReducer:StoreState}) => state.catFactsReducer);
  const dispatch = useDispatch();

  const getFacts = useCallback(
    () => dispatch({ type: Types.CAT_FACTS_REQUEST }),
    [dispatch]
  )

  const cleanFacts = useCallback(
    () => dispatch({ type: Types.CAT_FACTS_CLEAN_REQUEST }),
    [dispatch]
  )

  const rehydrateFacts = useCallback(
    () => dispatch({ type: Types.CAT_FACTS_REHYDRATE_REQUEST }),
    [dispatch]
  )

  /*useEffect(() => {
    if (!!catFactsStorage.has() && state.facts.length === 0) {
      console.log('rehydrating...')
      dispatch({ type: Types.CAT_FACTS_REHYDRATE_REQUEST })
    }
  }, [dispatch])*/

  return (
    <div>

      <h1>{title }</h1>

      <button onClick={getFacts}>
        Get Cat Facts! ⬇️ 
      </button>

      <button onClick={cleanFacts}>
        Clean Cat Facts 🧹
      </button>

      <button onClick={rehydrateFacts}>
        Rehydrate Cat Facts 💧
      </button>

      {
        state.loading && <div style={{ margin: 100 }}>
          <div className='spin'>
            🐈
        </div>
        </div>
      }

      {
        state.error && <div className='error'>
          <p>
            Error!
        </p>
          <p>
            {state.error}
          </p>
        </div>

      }

      <div>
        <h2>On Redux</h2>
        {
          state.facts && state.facts.map((el) =>
            <ul className='fact'>
              {el.text}
            </ul>
          )
        }
      </div>

      <div>
        <h2>On LocalStorage</h2>
        {
           (catFactsStorage.has() ?? []).map((el) =>
            <p className='fact'>
              {el.text}
            </p>
          )
        }
      </div>

      <hr/>

      <code>
        {JSON.stringify(state, null, '\t')}
      </code>

    </div>
  )
}
