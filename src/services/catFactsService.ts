import { AxiosResponse } from "axios";
import { catFactsApi } from "./api";
import { CatFactResponse } from '../../@types/CatFacts';

async function getCatFacts() : Promise<AxiosResponse<CatFactResponse>>{
  return catFactsApi.get(`/facts`)
}

const catFactsService = {
  getCatFacts
}

export default catFactsService