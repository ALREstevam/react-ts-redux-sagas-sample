import { CatFacts } from '../../@types/CatFacts';
import { getJsonFromLocalStorage } from '../utils/jsonLocalStorage';

enum LocalStorageKeys {
  CAT_FACTS = "@cat-facts-app/facts"
}

function store(facts: CatFacts[]) {
  try {
    localStorage.setItem(LocalStorageKeys.CAT_FACTS, JSON.stringify(facts));
  } catch (error) {
    console.log("Error: ", error);
  }
}

function has() {
  return getJsonFromLocalStorage<CatFacts[]>(LocalStorageKeys.CAT_FACTS)
}

function clean() {
  return localStorage.removeItem(LocalStorageKeys.CAT_FACTS)
}

const catFactsStorage = {
  store, has, clean
}

export default catFactsStorage

