import axios from 'axios'

const catFactsApi = axios.create({
  baseURL: 'https://cat-fact.herokuapp.com',
  timeout: 500,
});

export { catFactsApi }

