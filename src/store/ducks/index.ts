import { combineReducers } from 'redux';
import { catFactsReducer } from './catFactsActions'
const reducers = combineReducers({
  catFactsReducer
});

export default reducers;
