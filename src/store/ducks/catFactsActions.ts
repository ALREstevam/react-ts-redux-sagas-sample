import { CatFacts } from '../../../@types/CatFacts';
import { ReduxAction } from '../../../@types/Action';

// ACTION TYPES
export enum Types {
  CAT_FACTS_REQUEST = '@CatFacts/REQUEST',
  CAT_FACTS_SUCCEEDED = '@CatFacts/SUCCEEDED',
  CAT_FACTS_ERROR = '@CatFacts/ERROR',

  CAT_FACTS_CLEAN_REQUEST = '@CatFacts/CLEAN_REQUEST',
  CAT_FACTS_CLEAN_SUCCEEDED = '@CatFacts/CLEAN_SUCCEEDED',
  
  CAT_FACTS_REHYDRATE_REQUEST = '@CatFacts/REHYDRATE_REQUEST',
  CAT_FACTS_REHYDRATE_SUCCEEDED = '@CatFacts/REHYDRATE_SUCCEEDED',
}


// REDUCERS
export interface StoreState {
  facts: CatFacts[]
  error?: string
  loading: boolean
}

const INITIAL_STATE: StoreState = {
  facts: [],
  error: '',
  loading: false,
}

export const catFactsReducer = (
  state: StoreState = INITIAL_STATE,
  action: ReduxAction<Types, StoreState> = {}): StoreState => {

  console.log('REDUCER', action)

  switch (action.type) {
    case Types.CAT_FACTS_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      }

    case Types.CAT_FACTS_SUCCEEDED:
      return {
        ...state,
        facts: action.payload!.facts,
        loading: action.payload!.loading
      }

    case Types.CAT_FACTS_ERROR:
      return {
        ...state,
        facts: [],
        error: 'Error: ' + action.payload!.error,
        loading: action.payload!.loading
      }

    case Types.CAT_FACTS_CLEAN_REQUEST:
      return {
        ...state,
        loading: true
      }

    case Types.CAT_FACTS_CLEAN_SUCCEEDED:
      return INITIAL_STATE

    case Types.CAT_FACTS_REHYDRATE_REQUEST:
      return {
        ...state,
        loading: true
      }

    case Types.CAT_FACTS_REHYDRATE_SUCCEEDED:
      return {
        ...state,
        facts: action.payload!.facts,
        loading: false
      }

    default:
      return state
  }
}

// ACTION CREATORS
export const onCatFacts = (payload: StoreState) => {
  return { type: Types.CAT_FACTS_REQUEST, payload };
};