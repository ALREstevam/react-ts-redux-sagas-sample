import { put, call, takeEvery } from "redux-saga/effects";
import { Types as CatFactsTypes, StoreState as CatFactsPayload } from '../ducks/catFactsActions';
import { ReduxAction } from '../../../@types/Action';
import catFactsService from '../../services/catFactsService';
import catFactsStorage from '../../services/catFactsStorage';
import { AxiosResponse } from 'axios';
import { CatFactResponse } from '../../../@types/CatFacts';

function* catFactsSaga(action: ReduxAction<CatFactsTypes, CatFactsPayload>) {
  try {
    console.log('SAGA catFactsSaga')
    const response: AxiosResponse<CatFactResponse[]> =
      yield call(catFactsService.getCatFacts/*, action.payload*/);

    yield call(catFactsStorage.store, response.data);

    yield put({
      type: CatFactsTypes.CAT_FACTS_SUCCEEDED,
      payload: {
        facts: response.data,
        loading: false,
      }
    });
  } catch (error) {

    yield put({
      type: CatFactsTypes.CAT_FACTS_ERROR,
      payload: {
        error: error.response ? error.response.data : 'Error while loading cat facts',
        loading: false
      }
    });
  }
}


function* cleanCatFactsSaga(action: ReduxAction<CatFactsTypes, CatFactsPayload>) {
  try {
    console.log('SAGA cleanCatFactsSaga')
    yield call(catFactsStorage.clean);

    yield put({
      type: CatFactsTypes.CAT_FACTS_CLEAN_SUCCEEDED,
      payload: {
        facts: [],
        loading: false,
      }
    });
  } catch (error) {

    yield put({
      type: CatFactsTypes.CAT_FACTS_ERROR,
      payload: {
        error: error.response ? error.response.data : 'Error while cleaning cat facts',
        loading: false
      }
    });
  }
}

function* rehydrateCatFactsSaga(action: ReduxAction<CatFactsTypes, CatFactsPayload>) {
  try {
    console.log('SAGA rehydrateCatFactsSaga')
    const data : CatFactResponse[] = yield call(catFactsStorage.has);

    yield put({
      type: CatFactsTypes.CAT_FACTS_REHYDRATE_SUCCEEDED,
      payload: {
        facts: data,
        loading: false,
      }
    });
  } catch (error) {

    yield put({
      type: CatFactsTypes.CAT_FACTS_ERROR,
      payload: {
        error: error.response ? error.response.data : 'Error while rehydrating cat facts',
        loading: false
      }
    });
  }
}

export function* watchCatFactsSaga() {
  yield takeEvery(CatFactsTypes.CAT_FACTS_REQUEST, catFactsSaga);
  yield takeEvery(CatFactsTypes.CAT_FACTS_CLEAN_REQUEST, cleanCatFactsSaga)
  yield takeEvery(CatFactsTypes.CAT_FACTS_REHYDRATE_REQUEST, rehydrateCatFactsSaga)
}
