import { all } from 'redux-saga/effects';
import { watchCatFactsSaga } from './catFactsSaga';

export default function* rootSaga() {
  yield all([
    watchCatFactsSaga()
  ])
}